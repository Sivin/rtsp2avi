cmake_minimum_required(VERSION 3.10)
project(rtsp2avi)
set(CMAKE_CXX_STANDARD 11)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fstandalone-debug -g")

add_executable(rtsp2avi "")

target_sources(rtsp2avi PRIVATE
        src/main.cpp
        src/MediaOutput.cpp
        src/RtspInput.cpp
)

target_include_directories(rtsp2avi PRIVATE
        src
        ffmpeg/include
)

target_link_directories(rtsp2avi PRIVATE
        ffmpeg/lib
)


if (CMAKE_SYSTEM_NAME MATCHES "Darwin")
    target_link_libraries(rtsp2avi
            avformat
            swresample
            avcodec
            avutil
            pthread
            z
    )
else ()
    target_link_libraries(rtsp2avi
            avformat
            avcodec
            swresample
            avutil
            lzma
            bz2
            ssl
            crypto
            pthread
            z
    )
endif ()


