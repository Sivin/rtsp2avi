//
// Created by Sivin on 2024/5/11.
//

#ifndef RTSP2AVI_MEDIAPACKET_H
#define RTSP2AVI_MEDIAPACKET_H
extern "C" {
#include "libavformat/avformat.h"
}

class MediaPacket {
public:

  AVPacket pkt{};

  bool isVideo{false};

  bool isKeyFrame{};

  void release() {
    av_packet_unref(&pkt);
  }
};


#endif //RTSP2AVI_MEDIAPACKET_H
