//
// Created by Sivin on 2024/5/11.
//

#ifndef RTSP2AVI_RTSP_INPUT_H
#define RTSP2AVI_RTSP_INPUT_H

#include "MediaPacket.h"
#include <string>

class RtspInput {
public:
  explicit RtspInput(std::string url);
  ~RtspInput();

  int Open();

  int ReadPacket(MediaPacket &packet);

  AVRational& GetTimeBase();

  AVCodecParameters *GetCodecPar();

private:

  AVFormatContext *fmtCtx_{nullptr};

  std::string url_{};

  int videoStreamIndex_{-1};
};


#endif //RTSP2AVI_RTSP_INPUT_H
