//
// Created by Sivin on 2024/5/11.
//

#ifndef RTSP2AVI_MEDIA_OUTPUT_H
#define RTSP2AVI_MEDIA_OUTPUT_H

#include <string>
#include <iostream>
#include "MediaPacket.h"

class MediaOutput {
public:
  explicit MediaOutput(std::string savePath);

  ~MediaOutput();

  int Configure(AVCodecParameters *videoCodecPar);

  int Open();

  int WritePacket(MediaPacket &pkt, bool isVideo, const AVRational &inputTimeBase);

  int Close();

private:
  std::pair<std::string, std::string> SplitPath(const std::string &path);

private:
  int64_t startPts{-1};

  int64_t startDts{-1};

  AVFormatContext *fmtCtx{nullptr};

  std::string outFilePath_{};

  //视频记录的时长，视频时长可能会比这个时间要大
  int recoderTime_{0};

  int videoStreamIndex{-1};
};

#endif //RTSP2AVI_MEDIA_OUTPUT_H
