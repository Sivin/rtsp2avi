
CXXFLAGS = -std=c++11 -I./src -I./ffmpeg/include
LDFLAGS = -L./ffmpeg/lib -lavformat -lswresample -lavcodec -lavutil -lpthread -lz

OUT_DIR = bin

TARGET = $(OUT_DIR)/rtsp2avi

SOURCES = src/main.cpp src/MediaOutput.cpp src/RtspInput.cpp

OBJECTS = $(SOURCES:.cpp=.o)

all: $(OUT_DIR) $(TARGET)

$(OUT_DIR):
	mkdir -p $(OUT_DIR)

$(TARGET): $(OBJECTS)
	$(CXX) $(OBJECTS) $(LDFLAGS) -o $@

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	rm -f $(OBJECTS) $(TARGET)

.PHONY: all clean